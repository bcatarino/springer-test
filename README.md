# Springer Test - Drawing Canvas

## Chosen Technologies

I decided to choose Scala, simply because I find more elegant than Java and I'll take any opportunity to work more with it when there is no reason to use a specific technology.

## Things I'm not happy about

I feel like for the sake of immutability I broke encapsulation, and I'm not very happy with some of the original design choices I made.

## Not Implemented

It seemed like a very simple exercise, but as I should know by now, anything that deals with bi-dimensional arrays and coordinates of x and y can easily get confusing and cause ridiculous amounts of wasted time to figure out basic bugs. Especially when solving it in evenings after a tiring day of work.

Because I spent so much time having to sort out those issues, I ended up not finishing more than the first half of the exercise after several hours working on it. Hardly what I expected to achieve, but that's just how it is.

I didn't implement the square, the filling with colour, and the main command line App. The line and the basic model are implemented, with good test coverage, I believe.

## How I'd solve the remaining test if I had time

1) Square implementation: I'd simply use the same ShapeHandler class to draw the square based on the horizontal and vertical lines. I'd write some tests to verify drawing squares of different dimensions (side 1, side 2, and a larger one), as well as draw in middle and on the edges.
2) Filling implementation: I'd create a new class to keep fill separated from shapes (FillHandler) and I would create a recursive algorithm to go from the chosen point and expand in every direction (top, bottom, left, right) from the chosen position, verify if there is an x, and if not, "paint" it with the colour and repeat the recursion (tail recursion to avoid stackoverflow). I could probably keep a cache of visited points to avoid visiting the same points multiple times. I would assume painting on an 'x' (as starting point) is invalid (for simplicity).
3) Testing Filling implementation: Fill well defined shape without going out, fill shapes with an opening and make sure it creeps out of the shape, make sure it paints all the way to the edges of the canvas if no closed line of 'x' exists.
4) Console: I'd separate the main App object from the Console class. The App object would only receive the Array[String] parameters and delegate to the Console class. The console class would be responsible for parsing the arguments, returning errors if invalid arguments or command, and maybe create an instance of a Command class. I could have the CommandHandler class to receive a command and call the ShapeHandler and the FillHandler according to the command given by the user. The CommandHandler would contain the current Canvas object (not very functional though, but given the nature of the problem, I'd need to keep state somewhere).

## Conclusion

I'd like to finish it, but as much as I am interested in the role (and I am), I already spend a lot of time with the test, unfortunately without achieving the goal I expected in that amount of time. It happens, but I have to be practical. It's an exercise that can easily cause small bugs (with positions and array indexes, lots of validations), that can easily take a long time to figure out. Other recruitment processes I've been involved recently have asked for no more than 90 minutes to 2 hours, either a timed do at home timed exercise, or better yet, do it during a f2f interview where there's equal time commitment from both sides. I have to take that into account as well. Like Poker, there's a moment you have to cut your losses and fold.

I hope what I've done, plus some other tests I've done in the past and have on my github (https://github.com/bcatarino) can be enough to show my coding and can be enough to move to a next stage. Like I said, I'd welcome the chance to continue interviewing and hopefully get to work in a Kotlin project. If not, I completely understand, but ask you to also understand I can't commit any more hours on something that may or may not lead to a job, while ignoring everything else.

Thank you