package com.springer.canvas

import com.springer.canvas.model.EmptyCanvas
import org.scalatest.{Matchers, WordSpec}

import scala.collection.mutable.ArrayBuffer

class ShapeHandlerSpec extends WordSpec with Matchers {

  val testCanvas = EmptyCanvas(3, 4)

  val shapesHandler = new ShapeHandler

  "ShapeHandler#drawLine" should {
    "throw exception if startX is less than 0" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(-1, 0, 0, 0, testCanvas) should have message "requirement failed: startX should be between 0 and 3"
    }

    "throw exception if startY is less than 0" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(0, -1, 0, 0, testCanvas) should have message "requirement failed: startY should be between 0 and 4"
    }

    "throw exception if endX is less than 0" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(0, 0, -1, 0, testCanvas) should have message "requirement failed: endX should be between 0 and 3"
    }

    "throw exception if endY is less than 0" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(0, 0, 0, -1, testCanvas) should have message "requirement failed: endY should be between 0 and 4"
    }

    "throw exception if startX is more than width" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(3, 0, 0, 0, testCanvas) should have message "requirement failed: startX should be between 0 and 3"
    }

    "throw exception if startY is more than height" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(0, 4, 0, 0, testCanvas) should have message "requirement failed: startY should be between 0 and 4"
    }

    "throw exception if endX is more than width" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(0, 0, 3, 0, testCanvas) should have message "requirement failed: endX should be between 0 and 3"
    }

    "throw exception if endY is more than height" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(0, 0, 0, 4, testCanvas) should have message "requirement failed: endY should be between 0 and 4"
    }

    "throw exception if not horizontal or vertical" in {
      the [IllegalArgumentException] thrownBy shapesHandler.drawLine(2, 1, 1, 2, testCanvas) should have message "requirement failed: only horizontal and vertical lines are allowed"
    }

    "draw a point" in {
      val expected = ArrayBuffer(ArrayBuffer(' ', ' ', ' '), ArrayBuffer(' ', 'x', ' '), ArrayBuffer(' ', ' ', ' '), ArrayBuffer(' ', ' ', ' '))
      shapesHandler.drawLine(1, 1, 1, 1, testCanvas).rows shouldBe expected
    }

    "draw an horizontal line" in {
      val expected = ArrayBuffer(ArrayBuffer(' ', ' ', ' '), ArrayBuffer('x', 'x', 'x'), ArrayBuffer(' ', ' ', ' '), ArrayBuffer(' ', ' ', ' '))
      shapesHandler.drawLine(0, 1, 2, 1, testCanvas).rows shouldBe expected.map(_.toList).toList
    }

    "draw a vertical line" in {
      val expected = ArrayBuffer(ArrayBuffer(' ', ' ', 'x'), ArrayBuffer(' ', ' ', 'x'), ArrayBuffer(' ', ' ', 'x'), ArrayBuffer(' ', ' ', ' '))
      shapesHandler.drawLine(2, 0, 2, 2, testCanvas).rows shouldBe expected.map(_.toList).toList
    }

    "draw an horizontal line when start and end positions are reversed" in {
      val expected = ArrayBuffer(ArrayBuffer(' ', ' ', ' '), ArrayBuffer('x', 'x', 'x'), ArrayBuffer(' ', ' ', ' '), ArrayBuffer(' ', ' ', ' '))
      shapesHandler.drawLine(2, 1, 0, 1, testCanvas).rows shouldBe expected.map(_.toList).toList
    }

    "draw a vertical line when start and end positions are reversed" in {
      val expected = ArrayBuffer(ArrayBuffer(' ', ' ', 'x'), ArrayBuffer(' ', ' ', 'x'), ArrayBuffer(' ', ' ', 'x'), ArrayBuffer(' ', ' ', ' '))
      shapesHandler.drawLine(2, 2, 2, 0, testCanvas).rows shouldBe expected.map(_.toList).toList
    }
  }
}
