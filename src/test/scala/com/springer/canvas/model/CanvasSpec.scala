package com.springer.canvas.model

import org.scalatest.{Matchers, WordSpec}

import scala.collection.mutable.ArrayBuffer
import scala.util.Properties

class CanvasSpec extends WordSpec with Matchers {

  "Canvas#new" should {
    "throw exception if 0 rows when instantiating" in {
      intercept[IllegalArgumentException] { EmptyCanvas(0, 1) }
    }

    "throw exception if 0 columns when instantiating" in {
      intercept[IllegalArgumentException] { EmptyCanvas(1, 0) }
    }

    "throw exception if rows has wrong height" in {
      the [IllegalArgumentException] thrownBy new Canvas(2, 2, List()) should have message "requirement failed: the canvas array does not have the right height"
    }

    "throw exception if rows has wrong width" in {
      the [IllegalArgumentException] thrownBy new Canvas(2, 2, List(List(), List())) should have message "requirement failed: at least one of the rows of the canvas array doesn't have the right width"
    }

    "create Canvas with single position" in {
      EmptyCanvas(1, 1).getRows shouldBe ArrayBuffer(ArrayBuffer(' '))
    }

    "create Canvas for 2 for 3" in {
      EmptyCanvas(2, 3).getRows shouldBe ArrayBuffer.fill(3, 2)(' ')
    }

    "create Canvas when rows has right width and height" in {
      new Canvas(2, 3, List(List(' ', ' '), List(' ', ' '), List(' ', ' '))).getRows shouldBe ArrayBuffer.fill(3, 2)(' ')
    }
  }

  "Canvas#toString" should {
    "create string representation for 1 for 1 canvas" in {
      EmptyCanvas(1, 1).toString shouldBe Seq("---", "| |", "---").mkString(Properties.lineSeparator)
    }

    "create string representation for 2 for 3 canvas" in {
      EmptyCanvas(2, 3).toString shouldBe Seq("----", "|  |", "|  |", "|  |", "----").mkString(Properties.lineSeparator)
    }
  }
}
