package com.springer.canvas

import com.springer.canvas.model.Canvas

class ShapeHandler {

  def drawLine(startX: Int, startY: Int, endX: Int, endY: Int, canvas: Canvas): Canvas = {

    validateInputPoints(startX, startY, endX, endY, canvas.width, canvas.height)

    if (startX == endX) {
      drawHorizontalLine(startY, endY, startX, canvas)
    } else {
      drawVerticalLine(startX, endX, startY, canvas)
    }
  }

  // I'm not sure if I'm happy here to be breaking encapsulation to keep the Canvas model immutable.
  // I could refactor this and reuse some of this stuff too.
  private def drawVerticalLine(startY: Int, endY: Int, x: Int, canvas: Canvas): Canvas = {
    val updatedCanvas = canvas.getRows.map(_.toBuffer).toBuffer
    ascendingRange(startY, endY).foreach(y => updatedCanvas(x)(y) = 'x')
    canvas.copy(rows = updatedCanvas.map(_.toList).toList)
  }

  private def drawHorizontalLine(startX: Int, endX: Int, y: Int, canvas: Canvas): Canvas = {
    val updatedCanvas = canvas.getRows.map(_.toBuffer).toBuffer
    ascendingRange(startX, endX).foreach(x => updatedCanvas(x)(y) = 'x')
    canvas.copy(rows = updatedCanvas.map(_.toList).toList)
  }

  private def ascendingRange(a: Int, b: Int) = {
    val min = math.min(a, b)
    val max = math.max(a, b)
    min to max
  }

  private def validateInputPoints(startX: Int, startY: Int, endX: Int, endY: Int, width: Int, height: Int): Unit = {
    require(startX >= 0 && startX < width, s"startX should be between 0 and $width")
    require(startY >= 0 && startY < height, s"startY should be between 0 and $height")
    require(endX >= 0 && endX < width, s"endX should be between 0 and $width")
    require(endY >= 0 && endY < height, s"endY should be between 0 and $height")
    require(startX == endX || startY == endY, "only horizontal and vertical lines are allowed")
  }
}
