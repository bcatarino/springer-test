package com.springer.canvas.model

import com.springer.canvas.Point

import scala.util.Properties

case class EmptyCanvas(override val width: Int, override val height: Int) extends Canvas(width = width, height = height, rows = List.fill(height, width)(' '))

class Canvas(val width: Int, val height: Int, val rows: List[List[Point]]) {
  require(width > 0, "width must be at least 1")
  require(height > 0, "height must be at least 1")
  require(rows.lengthCompare(height) == 0, "the canvas array does not have the right height")
  require(rows.forall(_.lengthCompare(width) == 0), "at least one of the rows of the canvas array doesn't have the right width")

  def getRows: List[List[Point]] = rows

  def copy(width: Int = this.width, height: Int = this.height, rows: List[List[Point]] = this.rows) = new Canvas(width, height, rows)

  override def toString: String = {
    val topBottomRow = Seq.fill(width + 2)("-").mkString
    val midRows = rows.map(row => (Seq("|") ++ row ++ Seq("|")).mkString)
    (Seq(topBottomRow) ++ midRows ++ Seq(topBottomRow)).mkString(Properties.lineSeparator)
  }
}